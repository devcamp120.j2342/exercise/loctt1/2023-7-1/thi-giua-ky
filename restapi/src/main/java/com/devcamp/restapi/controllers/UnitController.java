package com.devcamp.restapi.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.devcamp.restapi.models.Unit;
import com.devcamp.restapi.repositories.ChapterRepository;
import com.devcamp.restapi.repositories.UnitRepository;
import com.devcamp.restapi.services.UnitService;

@RestController
@CrossOrigin
public class UnitController {
    @Autowired
    UnitRepository unitRepository;
    @Autowired
    ChapterRepository chapterRepository;
    @Autowired
    UnitService unitService;
    @GetMapping("/units/all")
    public ResponseEntity<List<Unit>> getAllUnit(){
        try{
            return new ResponseEntity<List<Unit>>(unitRepository.findAll(), HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/units/detail/{unitId}")
    public ResponseEntity<Unit> getUnitDetail(@PathVariable("unitId") long unitId){
        try{
            Optional<Unit> unit = unitRepository.findById(unitId);
            if(unit.isPresent()){
                return new ResponseEntity<Unit>(unit.get(), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch(Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/units/create/{chapterId}")
    public ResponseEntity<Object> createUnit(@PathVariable("chapterId") long chapterId,@Valid @RequestBody Unit unit){
        try{            
            return new ResponseEntity<>(unitService.createUnit(chapterId, unit), HttpStatus.CREATED);
        }catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/units/update/{id}")
    public ResponseEntity<Object> updateUnit(@PathVariable("id") long id,@Valid @RequestBody Unit unit){
        try{
            Optional<Unit> unitData = unitRepository.findById(id);
            if(unitData.isPresent()) {
                return new ResponseEntity<>(unitService.updateUnit(unitData.get(), unit), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/units/delete/{id}")
    public ResponseEntity<Object> deleteUnit(@PathVariable("id") long id){
        try{
            Optional<Unit> unit = unitRepository.findById(id);
            if(unit.isPresent()){
                unitRepository.deleteById(id);
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        }catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/units/delete/all")
    public ResponseEntity<Object> deleteAllChapter(){
        try{
            unitRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch(Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
