package com.devcamp.restapi.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapi.models.Chapter;
import com.devcamp.restapi.repositories.ChapterRepository;
import com.devcamp.restapi.services.ChapterService;

@RestController
@CrossOrigin
public class ChapterController {
    @Autowired
    ChapterRepository chapterRepository;
    @Autowired
    ChapterService chapterService;
    @GetMapping("/chapters/all")
    public ResponseEntity<List<Chapter>> getAllChapter() {
        try {
            return new ResponseEntity<List<Chapter>>(chapterRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/chapters/detail/{chapterId}")
    public ResponseEntity<Chapter> getChapterDetail(@PathVariable("chapterId") long chapterId){
        try{
            Optional<Chapter> chapter = chapterRepository.findById(chapterId);
            if(chapter.isPresent()){
                return new ResponseEntity<>(chapter.get(), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        }catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/chapters/create")
    public ResponseEntity<Object> createChapter(@Valid @RequestBody Chapter chapter) {
        try{
            
            return new ResponseEntity<>(chapterService.createChapter(chapter), HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/chapters/update/{chapterId}")
    public ResponseEntity<Object> updateChapter(@PathVariable("chapterId") long chapterId,@Valid @RequestBody Chapter chapter){
        try{
            Optional<Chapter> chapterData = chapterRepository.findById(chapterId);
            if(chapterData.isPresent()){
                return new ResponseEntity<>(chapterService.updateChapter(chapterData.get(), chapter), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch(Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/chapters/delete/{chapterId}")
    public ResponseEntity<Object> deleteChapter(@PathVariable("chapterId") long chapterId){
        try{
            Optional<Chapter> chapter = chapterRepository.findById(chapterId);
            if(chapter.isPresent()){
                chapterRepository.deleteById(chapterId);
                return new ResponseEntity<Object>(null, HttpStatus.NO_CONTENT);
            }else{
                return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
            }
        }catch(Exception e){
            return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/chapters/delete/all")
    public ResponseEntity<Object> deleteAllChapter(){
        try{
            chapterRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }catch(Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
