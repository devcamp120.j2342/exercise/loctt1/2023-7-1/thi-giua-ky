package com.devcamp.restapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.restapi.models.Chapter;

public interface ChapterRepository extends JpaRepository<Chapter, Long> {
    
}
