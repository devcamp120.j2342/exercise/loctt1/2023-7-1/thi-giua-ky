package com.devcamp.restapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.restapi.models.Unit;

public interface UnitRepository extends JpaRepository<Unit, Long> {
    
}
