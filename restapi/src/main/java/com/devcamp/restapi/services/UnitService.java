package com.devcamp.restapi.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.restapi.models.Chapter;
import com.devcamp.restapi.models.Unit;
import com.devcamp.restapi.repositories.ChapterRepository;
import com.devcamp.restapi.repositories.UnitRepository;

@Service
public class UnitService {
    @Autowired
    UnitRepository unitRepository;
    @Autowired
    ChapterRepository chapterRepository;
    public Unit createUnit(Long chapterId, Unit unit ) {
        Optional<Chapter> chapter = chapterRepository.findById(chapterId);
        Unit newRole = new Unit();
        newRole.setUnitCode(unit.getUnitCode());
        newRole.setUnitName(unit.getUnitName());
        newRole.setIntroduction(unit.getIntroduction());
        newRole.setTotalPage(unit.getTotalPage());
        if(chapter.isPresent()){
            newRole.setChapter(chapter.get());
        }
        Unit saveUnit = unitRepository.save(newRole);
        return saveUnit;
    }

    public Unit updateUnit(Unit unitData, Unit unitUpdateData){
        Unit _unit = unitData;
        _unit.setUnitCode(unitUpdateData.getUnitCode());
        _unit.setUnitName(unitUpdateData.getUnitName());
        _unit.setIntroduction(unitUpdateData.getIntroduction());
        _unit.setTotalPage(unitUpdateData.getTotalPage());
        Unit saveUnit = unitRepository.save(_unit);
        return saveUnit;
    }
}
