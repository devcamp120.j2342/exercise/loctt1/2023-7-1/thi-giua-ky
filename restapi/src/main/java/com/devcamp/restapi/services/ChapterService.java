package com.devcamp.restapi.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.restapi.models.Chapter;
import com.devcamp.restapi.repositories.ChapterRepository;

@Service
public class ChapterService {
    @Autowired
    ChapterRepository chapterRepository;

    public Chapter createChapter(Chapter chapter) {
        Chapter newRole = new Chapter();
        newRole.setChapterCode(chapter.getChapterCode());
        newRole.setChapterName(chapter.getChapterName());
        newRole.setIntroduction(chapter.getIntroduction());
        newRole.setTranslatorName(chapter.getTranslatorName());
        newRole.setTotalPage(chapter.getTotalPage());
        Chapter saveChapter = chapterRepository.save(newRole);
        return saveChapter;
    }

    public Chapter updateChapter(Chapter chapterData, Chapter newChapterData){
        Chapter _chapter = chapterData;
        _chapter.setChapterCode(newChapterData.getChapterCode());
        _chapter.setChapterName(newChapterData.getChapterName());
        _chapter.setIntroduction(newChapterData.getIntroduction());
        _chapter.setTranslatorName(newChapterData.getTranslatorName());
        _chapter.setTotalPage(newChapterData.getTotalPage());
        Chapter saveChapter = chapterRepository.save(_chapter);
        return saveChapter;
    }
}
