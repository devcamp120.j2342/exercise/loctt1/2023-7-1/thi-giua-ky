package com.devcamp.restapi.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "chapters")
public class Chapter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotEmpty(message = "Nhập mã chương")
    @Column(name = "chapter_code")
    private String chapterCode;
    @NotEmpty(message = "Nhập nhập tên chương")
    @Column(name = "chapter_name")
    private String chapterName;
    @Column
    private String introduction;
    @NotEmpty(message = "Nhập tên người dịch")
    @Column(name = "translator_name")
    private String translatorName;
    @NotNull(message = "Nhập số lượng trang")
    @Range(min=1, message = "Nhập giá trị nhỏ nhất là 1")
    @Column(name = "total_pages")
    private int totalPage;
    @OneToMany(mappedBy = "chapter", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Unit> units;

    public Chapter() {
    }
    
    public Chapter(long id, String chapterCode, String chapterName, String introduction, String translatorName,
            int totalPage, List<Unit> units) {
        this.id = id;
        this.chapterCode = chapterCode;
        this.chapterName = chapterName;
        this.introduction = introduction;
        this.translatorName = translatorName;
        this.totalPage = totalPage;
        this.units = units;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getChapterCode() {
        return chapterCode;
    }

    public void setChapterCode(String chapterCode) {
        this.chapterCode = chapterCode;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getTranslatorName() {
        return translatorName;
    }

    public void setTranslatorName(String translatorName) {
        this.translatorName = translatorName;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public List<Unit> getUnits() {
        return units;
    }

    public void setUnits(List<Unit> units) {
        this.units = units;
    }

}
