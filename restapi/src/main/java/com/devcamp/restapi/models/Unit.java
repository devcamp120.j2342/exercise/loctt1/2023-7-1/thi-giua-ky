package com.devcamp.restapi.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "units")
public class Unit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotEmpty(message = "Nhập mã bài")
    @Column(name =  "unit_code")
    private String unitCode;
    @NotEmpty(message = "Nhập tên bài")
    @Column(name = "unit_name")
    private String unitName;
    @Column
    private String introduction;
    @NotNull(message = "Nhập tổng số trang")
    @Column(name = "total_pages")
    private int totalPage;
    @ManyToOne
    @JoinColumn(name = "chapter_id")
    private Chapter chapter;

    public Unit(long id, String unitCode, String unitName, String introduction, int totalPages, Chapter chapter) {
        this.id = id;
        this.unitCode = unitCode;
        this.unitName = unitName;
        this.introduction = introduction;
        this.totalPage = totalPages;
        this.chapter = chapter;
    }

    public Unit() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnitCode() {
        return unitCode;
    }

    public void setUnitCode(String unitCode) {
        this.unitCode = unitCode;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPages) {
        this.totalPage = totalPages;
    }

    public Chapter getChapter() {
        return chapter;
    }

    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }

}
