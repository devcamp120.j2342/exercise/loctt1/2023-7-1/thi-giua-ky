$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC, gọi hàm khi load */
    var gChapterTable = $("#chapter-table").DataTable({
        columns: [
            { data: 'id' },
            { data: 'chapterCode' },
            { data: 'chapterName' },
            { data: 'translatorName' },
            { data: 'totalPage' },
            { data: 'action' },
        ],
        columnDefs: [
            {
                targets: -1,
                defaultContent: `<i class="fas fa-edit text-primary"></i>
              | <i class="fas fa-trash text-danger"></i>`,
            },
        ],
    })
    const gUrl = "http://localhost:8080/chapters/";
    var gChapterId = 0;
    /*** Region 2: gán hàm xử lý sự kiện ***/
    onPageLoading();
    //Gán sk nút C:
    $("#btn-create-chapter").click(function () {
        onBtnCreateChapterClick();
    });
    //Gán sk nút U:
    $("#btn-update-chapter").click(function () {
        onBtnUpdateChapterClick();
    });
    //Gán sk nút D:
    $("#btn-delete-chapter").click(function () {
        onBtnDeleteChapterClick();
    });
    //Gán sk nút Delete all:
    $("#btn-delete-all-chapter").click(function () {
        $("#modal-confirm-delete").modal("show");
        gChapterId = 0;
    });
    //Gán sk nút icon edit:
    $("#chapter-table").on("click", ".fa-edit", function () {
        loadDataChapterToForm(this);
    });
    //Gán sk nút icon trash:
    $("#chapter-table").on("click", ".fa-trash", function () {
        onDeleteChapterByIdClick(this);
    });
    /*** Region 3: hàm xử lý sự kiện  */
    //Hàm xử lý khi load trang:
    function onPageLoading() {
        $.ajax({
            url: gUrl + "all",
            type: "GET",
            dataType: "JSON",
            success: function (response) {
                loadDataChapterToTable(response);
            },
            error: function (error) {
                alert(error.responseText);
            }
        })

    }
    //hàm xử lý nút tạo chương click:
    function onBtnCreateChapterClick() {
        //B1:Thu thập dữ liệu:
        var vChapterObj = {
            chapterCode: "",
            chapterName: "",
            introduction: "",
            translatorName: "",
            totalPage: ""
        }
        getDataChapter(vChapterObj);
        //B2: KT dữ liệu
        var vValid = validateDataChapter(vChapterObj);
        if (vValid) {
            //gọi server:
            $.ajax({
                url: gUrl + "create",
                type: "POST",
                dataType: "JSON",
                contentType: "application/json",
                data: JSON.stringify(vChapterObj),
                success: function (response) {
                    alert('Chapter created successfully');
                    onPageLoading();
                    resetChapterInput();
                }
            })
        }
    }
    //hàm xử lý nút tạo chương click:
    function onBtnUpdateChapterClick() {
        //B1:Thu thập dữ liệu:
        var vChapterObj = {
            chapterCode: "",
            chapterName: "",
            introduction: "",
            translatorName: "",
            totalPage: ""
        }
        getDataChapter(vChapterObj);
        //B2: KT dữ liệu
        var vValid = validateDataChapter(vChapterObj);
        if (vValid) {
            //gọi server:
            $.ajax({
                url: gUrl + "update/" + gChapterId,
                type: "PUT",
                dataType: "JSON",
                contentType: "application/json",
                data: JSON.stringify(vChapterObj),
                success: function (response) {
                    alert('Chapter update successfully');
                    onPageLoading();
                    resetChapterInput();

                },
                error: function (response) {
                    alert(response.responseText);
                }
            })
        }
    }
    //Hàm xử lý icon trash click:
    function onDeleteChapterByIdClick(paramElement) {
        var vSelectedRow = $(paramElement).parents("tr");
        var vDataRow = gChapterTable.row(vSelectedRow).data();
        gChapterId = vDataRow.id;
        $("#modal-confirm-delete").modal("show");
    }
    //Hàm xử lý nýt xác nhận xóa chương;
    function onBtnDeleteChapterClick() {
        if(gChapterId == 0){
            $.ajax({
                url: gUrl + "delete/all",
                type: "DELETE",
                dataType: "JSON",
                success: function (response) {
                    alert('All chapter delete successfully');
                    $("#modal-confirm-delete").modal("hide");
                    onPageLoading();
                },
                error: function (response) {
                    alert(response.responseText);
                }
            })
        }else{
            $.ajax({
                url: gUrl + "delete/" + gChapterId,
                type: "DELETE",
                dataType: "JSON",
                success: function (response) {
                    alert('Chapter delete successfully');
                    $("#modal-confirm-delete").modal("hide");
                    onPageLoading();
                },
                error: function (response) {
                    alert(response.responseText);
                }
            })
        }
        
    }
    /*** Region 4: hàm dùng chung*/
    //Hàm loadData ra bảng;
    function loadDataChapterToTable(paramChapters) {
        'use strict';
        gChapterTable.clear();
        gChapterTable.rows.add(paramChapters);
        gChapterTable.draw();
    }
    //Hàm lấy dự liệu chương từ form:
    function getDataChapter(paramChapter) {
        paramChapter.chapterCode = $("#input-chapter-code").val().trim();
        paramChapter.chapterName = $("#input-chapter-name").val().trim();
        paramChapter.introduction = $("#input-introduction").val().trim();
        paramChapter.translatorName = $("#input-translator").val().trim();
        paramChapter.totalPage = $("#input-total-page").val().trim();
    }
    //Hàm kiểm tra dữ liệu chương được nhập vào:
    function validateDataChapter(paramChapter) {
        if (paramChapter.chapterCode == "") {
            alert("Hãy nhập mã chương");
            return false;
        }
        if (paramChapter.chapterName == "") {
            alert("Hãy nhập tên chương");
            return false;
        }
        if (paramChapter.translatorName == "") {
            alert("Hãy nhập tên người dịch");
            return false;
        }
        if (paramChapter.totalPage == null) {
            alert("Hãy nhập tổng số trang");
            return false;
        }
        return true;
    }
    //Hàm reset form :
    function resetChapterInput() {
        $("#input-chapter-code").val(null);
        $("#input-chapter-name").val(null);
        $("#input-introduction").val(null);
        $("#input-translator").val(null);
        $("#input-total-page").val(null);
    }
    //Hàm load dữ liệu chương ra form:
    function loadDataChapterToForm(paramElement) {
        var vSelectedRow = $(paramElement).parents("tr");
        var vDataRow = gChapterTable.row(vSelectedRow).data();
        gChapterId = vDataRow.id;
        $.ajax({
            url: gUrl + "detail/" + gChapterId,
            type: "GET",
            dataType: "JSON",
            success: function (response) {
                $("#input-chapter-code").val(response.chapterCode);
                $("#input-chapter-name").val(response.chapterName);
                $("#input-introduction").val(response.introduction);
                $("#input-translator").val(response.translatorName);
                $("#input-total-page").val(response.totalPage);
            },
            error: function (response) {
                alert(response.responseText);
            }
        })
    }

});