$(document).ready(function () {
"use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC, gọi hàm khi load */
    var gUnitTable = $("#unit-table").DataTable({
        columns: [
            { data: 'id' },
            { data: 'unitCode' },
            { data: 'unitName' },
            { data: 'totalPage' },
            { data: 'action' },
        ],
        columnDefs: [
            {
                targets: -1,
                defaultContent: `<i class="fas fa-edit text-primary"></i>
              | <i class="fas fa-trash text-danger"></i>`,
            },
        ],
    })
    const gUnitUrl = "http://localhost:8080/units/";
    const gChapterUrl = "http://localhost:8080/chapters/";
    var gUnitId = 0;
    /*** Region 2: gán hàm xử lý sự kiện ***/
    onPageLoading();
    //Gán sk nút C:
    $("#btn-create-unit").click(function () {
        onBtnCreateUnitClick();
    });
    //Gán sk nút U:
    $("#btn-update-unit").click(function () {
        onBtnUpdateUnitClick();
    });
    //Gán sk nút D:
    $("#btn-delete-unit").click(function () {
        onBtnDeleteUnitClick();
    });
    //Gán sk nút Delete all:
    $("#btn-delete-all-unit").click(function () {
        $("#modal-confirm-delete").modal("show");
        gUnitId = 0;
    });
    //Gán sk nút icon edit:
    $("#unit-table").on("click", ".fa-edit", function () {
        loadDataUnitToForm(this);
    });
    //Gán sk nút icon trash:
    $("#unit-table").on("click", ".fa-trash", function () {
        onDeleteUnitByIdClick(this);
    });
    /*** Region 3: hàm xử lý sự kiện  */
    //Hàm xử lý khi load trang:
    function onPageLoading() {
        loadChapterSelect();
        loadDataUnitToTable();


    }
    //hàm xử lý nút tạo chương click:
    function onBtnCreateUnitClick() {
        //B1:Thu thập dữ liệu:
        var vUnitObj = {
            unitCode: "",
            unitName: "",
            introduction: "",
            totalPage: ""
        }
        var vChapterId = $("#select-chapter").val();
        getDataUnit(vUnitObj);
        //B2: KT dữ liệu
        var vValid = validateDataUnit(vUnitObj);
        if (vValid) {
            //gọi server:
            $.ajax({
                url: gUnitUrl + "create/" + vChapterId,
                type: "POST",
                dataType: "JSON",
                contentType: "application/json",
                data: JSON.stringify(vUnitObj),
                success: function () {
                    alert('Unit created successfully');
                    loadDataUnitToTable();
                    resetUnitInput();
                }
            })
        }
    }
    //hàm xử lý nút tạo chương click:
    function onBtnUpdateUnitClick() {
        //B1:Thu thập dữ liệu:
        var vUnitObj = {
            unitCode: "",
            unitName: "",
            introduction: "",
            totalPage: ""
        }
        getDataUnit(vUnitObj);
        //B2: KT dữ liệu
        var vValid = validateDataUnit(vUnitObj);
        if (vValid) {
            //gọi server:
            $.ajax({
                url: gUnitUrl + "update/" + gUnitId,
                type: "PUT",
                dataType: "JSON",
                contentType: "application/json",
                data: JSON.stringify(vUnitObj),
                success: function (response) {
                    alert('Unit update successfully');
                    loadDataUnitToTable();
                    resetUnitInput();

                },
                error: function (response) {
                    alert(response.responseText);
                }
            })
        }
    }
    //Hàm xử lý icon trash click:
    function onDeleteUnitByIdClick(paramElement) {
        var vSelectedRow = $(paramElement).parents("tr");
        var vDataRow = gUnitTable.row(vSelectedRow).data();
        gUnitId = vDataRow.id;
        $("#modal-confirm-delete").modal("show");
    }
    //Hàm xử lý nýt xác nhận xóa chương;
    function onBtnDeleteUnitClick() {
        if (gUnitId == 0) {
            $.ajax({
                url: gUnitUrl + "delete/all",
                type: "DELETE",
                dataType: "JSON",
                success: function (response) {
                    alert('All unit delete successfully');
                    $("#modal-confirm-delete").modal("hide");
                    loadDataUnitToTable();
                },
                error: function (response) {
                    alert(response.responseText);
                }
            })
        } else {
            $.ajax({
                url: gUnitUrl + "delete/" + gUnitId,
                type: "DELETE",
                dataType: "JSON",
                success: function (response) {
                    alert('Unit delete successfully');
                    $("#modal-confirm-delete").modal("hide");
                    loadDataUnitToTable();
                },
                error: function (response) {
                    alert(response.responseText);
                }
            })
        }

    }
    /*** Region 4: hàm dùng chung*/
    //Hàm loadData ra bảng;
    function loadDataUnitToTable() {
        $.ajax({
            url: gUnitUrl + "all",
            type: "GET",
            dataType: "JSON",
            success: function (response) {
                gUnitTable.clear();
                gUnitTable.rows.add(response);
                gUnitTable.draw();
            },
            error: function (error) {
                alert(error.responseText);
            }
        })
    }
    //hàm load dữ liệu chapter ra select:
    function loadChapterSelect(){
        var vChapterSelect = $("#select-chapter");
        $.ajax({
            url: gChapterUrl + "all",
            type: "GET",
            dataType: "JSON",
            success: function (response) {
                for(var bI = 0; bI < response.length; bI++) {
                    $("<option></option>").val(response[bI].id)
                        .text(response[bI].chapterName)
                        .appendTo(vChapterSelect);
                }
            },
            error: function (error) {
                alert(error.responseText);
            }
        })
    }

    //Hàm lấy dự liệu chương từ form:
    function getDataUnit(paramUnit) {
        paramUnit.unitCode = $("#input-unit-code").val().trim();
        paramUnit.unitName = $("#input-unit-name").val().trim();
        paramUnit.introduction = $("#input-introduction").val().trim();
        paramUnit.totalPage = $("#input-total-page").val().trim();
    }
    //Hàm kiểm tra dữ liệu chương được nhập vào:
    function validateDataUnit(paramUnit) {
        if (paramUnit.unitCode == "") {
            alert("Hãy nhập mã chương");
            return false;
        }
        if (paramUnit.unitName == "") {
            alert("Hãy nhập tên chương");
            return false;
        }
        if (paramUnit.totalPage == null) {
            alert("Hãy nhập tổng số trang");
            return false;
        }
        return true;
    }
    //Hàm reset form :
    function resetUnitInput() {
        $("#input-unit-code").val(null);
        $("#input-unit-name").val(null);
        $("#input-introduction").val(null);
        $("#select-chapter").val(0);
        $("#input-total-page").val(null);
    }
    //Hàm load dữ liệu chương ra form:
    function loadDataUnitToForm(paramElement) {
        var vSelectedRow = $(paramElement).parents("tr");
        var vDataRow = gUnitTable.row(vSelectedRow).data();
        gUnitId = vDataRow.id;
        $.ajax({
            url: gUnitUrl + "detail/" + gUnitId,
            type: "GET",
            dataType: "JSON",
            success: function (response) {
                $("#input-unit-code").val(response.unitCode);
                $("#input-unit-name").val(response.unitName);
                $("#input-introduction").val(response.introduction);
                $("#select-chapter").val(response.chapter.id);
                $("#input-total-page").val(response.totalPage);
            },
            error: function (response) {
                alert(response.responseText);
            }
        })
    }

});